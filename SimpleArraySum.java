import java.io.*;
import java.util.*;

class SimpleArraySum {
  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int n=sc.nextInt();
    int i=0,sum=0;
    while(i<n){
      sum=sum+sc.nextInt();
      i++;
    }
    System.out.println(sum);
  }
}