import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class SaveThePrisoner {

    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        int t = sc.nextInt();
        int n, m, s, result;
        for(int i = 0; i < t; i++){
            n = sc.nextInt();
            m = sc.nextInt();
            s = sc.nextInt();
            result = (m+s-1)%n;
            if(result == 0)
                System.out.println(n);
            else
                System.out.println(result);
        }
    }
}