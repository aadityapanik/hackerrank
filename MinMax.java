import java.util.Scanner;

public class MinMax {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
	    long[] numbers = new long[5];
	    long a = sc.nextLong();
        long b = sc.nextLong();
        long c = sc.nextLong();
        long d = sc.nextLong();
        long e = sc.nextLong();
        insertNum(a, numbers);
        insertNum(b, numbers);
        insertNum(c, numbers);
        insertNum(d, numbers);
        insertNum(e, numbers);
        long minNumber = numbers[1]+numbers[2]+numbers[3]+numbers[4];
        long maxNumber = numbers[0]+numbers[1]+numbers[2]+numbers[3];
        System.out.println(minNumber+" "+maxNumber);
    }

    static void insertNum(long number, long[] numbers){
        int i;
        for(i=0; i<4; i++){
            if(numbers[i]<number)
                break;
        }
        for(int j=3; j>=i; j--){
            numbers[j+1]=numbers[j];
        }
        numbers[i]=number;
    }
}
