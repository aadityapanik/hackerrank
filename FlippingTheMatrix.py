def query(n):
	matrix = []
	for x in range(2*n):
		t_list = []
		for y in input().strip().split(' '):
			t_list.append(int(y))
		matrix.append(t_list)
	sum = 0
	for x in range(n):
		for y in range(n):
			q1 = matrix[0 + x][0 + y]
			q2 = matrix[0 + x][2*n-1 - y]
			q3 = matrix[2*n-1 - x][0 + y]
			q4 = matrix[2*n-1 - x][2*n-1 - y]
			sum += max(q1,q2,q3,q4)
	return sum

q = int(input())
for x in range(q):
	n = int(input())
	print(query(n))