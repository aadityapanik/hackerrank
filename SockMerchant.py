#!/bin/python3

import sys

n = int(input())
c = [int(c_temp) for c_temp in input().strip().split(' ')]

list = [[False]] * 100
count = 0

for i in c:
	if not list[(i-1)]:
		list[(i-1)] = not list[(i-1)]
		count += 1
		continue
	else:
		list[(i-1)] = not list[(i-1)]
		continue

print(count)