import java.io.*;
import java.util.*;

public class NonDivisibleSubset {

    public static void main(String[] args) {
        int n=0,k=0, tmpn=0, tmpr=0, setcount=0;
        int[] remainders = null;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        k = sc.nextInt();
        remainders = new int[k];
        for(int i=0; i<n; i++){
            tmpn = sc.nextInt();
            tmpr = tmpn%k;
            remainders[tmpr]++;
        }
        if(k>1){
            if(k%2!=0){
                for(int i=1; i<=(k/2); i++){
                    int j=k-i;
                    if(remainders[i]>remainders[j])
                        setcount+=remainders[i];
                    else if(remainders[i]<remainders[j])
                        setcount+=remainders[j];
                    else
                        setcount+=remainders[i];
                }
            }else{
                for(int i=1; i<(k/2); i++){
                    int j=k-i;
                    if(remainders[i]>remainders[j])
                        setcount+=remainders[i];
                    else if(remainders[i]<remainders[j])
                        setcount+=remainders[j];
                    else
                        setcount+=remainders[i];
                }
                setcount += 1;
            }
            if(remainders[0]>0)
                setcount++;
            System.out.println(setcount);
        }
        else
            System.out.println("1");
    }
}