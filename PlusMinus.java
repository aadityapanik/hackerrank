import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class PlusMinus {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int t=in.nextInt();
        int[] n=new int[3];
        int temp=0;
        for(int i=0;i<t;i++){
            temp=in.nextInt();
            if(temp>0)
                n[0]++;
            else if(temp<0)
                n[1]++;
            else
                n[2]++;
        }
        System.out.println((float)n[0]/(float)t);
        System.out.println((float)n[1]/(float)t);
        System.out.println((float)n[2]/(float)t);
    }
}