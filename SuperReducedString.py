import re
s = []
s.append(input())

isDone = False

while not isDone:
	s = re.subn(r'(\w)\1{1}', r'', s[0])
	if s[1] == 0 or s[0] == '':
		isDone = True

if s[0] == '':
	print("Empty String")
else:
	print(s[0])