def combinations(n, k, b):
	sum = 0
	boxes = []
	if b>k:
		print("-1")
	else:
		for i in range(1, b+1):
			boxes.append(i)
			sum += i
		if sum > n:
			print("-1")
			return
		for i in range(b-1, -1, -1):
			temp = min(n-sum, k-b)
			sum += temp
			boxes[i] += temp
		if sum != n:
			print("-1")
			return
		for i in range(0, b-1):
			print(boxes[i], end=' ')
		print(boxes[b-1])

t = int(input())
for x in range(t):
	n, k, b = [int(k) for k in input().split()]
	combinations(n, k, b)