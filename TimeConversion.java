import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;

public class TimeConversion {
  public static void main(String[] args) {
    try{
      Scanner sc = new Scanner(System.in);
      SimpleDateFormat source = new SimpleDateFormat("hh:mm:ssaa");
      SimpleDateFormat result = new SimpleDateFormat("HH:mm:ss");
      Date obj = source.parse(sc.nextLine());
      System.out.println(result.format(obj));
    } catch(Exception e){
      e.printStackTrace();
    }
  }
}