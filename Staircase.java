import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Staircase {

    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int l=in.nextInt();
        for(int i=1;i<=l;i++){
            for(int j=l;j>=1;j--){
                if(i>=j)
                    System.out.print("#");
                else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }
}