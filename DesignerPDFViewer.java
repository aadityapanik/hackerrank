import java.util.Scanner;

public class DesignerPDFViewer {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = 26;
        int h[] = new int[n];
        for(int h_i=0; h_i < n; h_i++){
            h[h_i] = in.nextInt();
        }
        String word = in.next();
        int area = wordArea(word, h);
        System.out.println(area);
    }

    static int wordArea(String word, int[] height_array) {
        int w_length = word.length();
        int max_height = 0;
        for(int i=0; i<w_length; i++) {
            int c_index = (int)word.charAt(i)-97;
            if(max_height<height_array[c_index])
                max_height = height_array[c_index];
        }
        return w_length*max_height;
    }
}
