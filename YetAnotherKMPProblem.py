import string

weights = list(map(int, input().split(' ')))
alp_list = map(list, filter(lambda s: s[0] != 0, zip(weights, string.ascii_lowercase)))
alp_list = sorted(ys)

c = alp_list[0][1]
alp_list[0][0] -= 1
if alp_list[0][0] == 0:
	del alp_list[0]
alp_list = list(sorted(alp_list, key=lambda p: p[1]))

s = [c]
while alp_list:
	i = 0
	if(len(s)>=2 and len(alp_list)>=2 and s[0] == s[1] == s[-1] == c == alp_list[i][1]):
		i = 1
	s.append(alp_list[i][1])
	alp_list[i][0] -= 1
	if alp_list[i][0] == 0:
		del alp_list[i]
print(*s, sep='')
