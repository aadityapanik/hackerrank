import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class DiagonalDifference {

    static int solve(int[][] m,int len) {
        int pd=0,nd=0;
        for(int i=0;i<len;i++){
            pd=pd+m[i][i];
            nd=nd+m[len-1-i][i];
        }
        return Math.abs(pd-nd);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t;
        int sum;
        int[][] matrix;
        t=in.nextInt();
        matrix=new int[t][t];
        for(int i=0;i<t;i++)
            for(int j=0;j<t;j++)
                matrix[i][j] = in.nextInt();
        System.out.println(solve(matrix,t));
    }
}