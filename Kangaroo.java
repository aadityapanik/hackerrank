import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Kangaroo {
    
    public static void doTheyMeet(int x1, int v1, int x2, int v2){
        int iterations = 0;
        int t1 = x1, t2 = x2;
        if((x2 > x1 && v2 > v1) || (x1 > x2 && v1 > v2)){
            System.out.println("NO");
            return;
        }
        if(x1 < x2){
            while(t1 < t2){
                t1 = t1 + v1;
                t2 = t2 + v2;
                //System.out.println("t1 = "+t1+"\nt2 = "+t2+"\n");
                if(t1 == t2){
                    System.out.println("YES");
                    return;
                }
            }
        }
        else{
            while(t1 > t2){
                t1 = t1 + v1;
                t2 = t2 + v2;
                //System.out.println("t1 = "+t1+"\nt2 = "+t2+"\n");
                if(t1 == t2){
                    System.out.println("YES");
                    return;
                }
            }
        }
        System.out.println("NO");
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x1 = in.nextInt();
        int v1 = in.nextInt();
        int x2 = in.nextInt();
        int v2 = in.nextInt();
        doTheyMeet(x1,v1,x2,v2);
    }
}