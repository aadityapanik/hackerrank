import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class StrangeCounter {
    
    static public long strangeCounter(long n){
        long temp = n/3, limit = 1, limitRadix = 0;
        temp = temp + 1;
        while(limit <= temp){
            limit = limit * 2;
            limitRadix++;
        }
        limit = (long)(3*(Math.pow(2,limitRadix)-1));
        return (limit-n+1);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long t = in.nextLong();
        System.out.println(strangeCounter(t));
    }
}
