import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class JumpingOnTheClouds {
    
    public static int numberOfJumps(int[] clouds){
        int len = clouds.length;
        int position = 0, jump = 0;
        while(position<(len-1)){
            if((position+2)<=(len-1)){
                if(clouds[position+2]==0){
                    position+=2;
                    jump++;
                    continue;
                }
                else{
                    position++;
                    jump++;
                }
            }
            else{
                position++;
                jump++;
            }
        }
        return jump;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int c[] = new int[n];
        for(int c_i=0; c_i < n; c_i++){
            c[c_i] = in.nextInt();
        }
        System.out.println(numberOfJumps(c));
    }
}
