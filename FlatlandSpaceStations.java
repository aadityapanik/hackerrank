import java.io.*;
import java.util.*;

public class FlatlandSpaceStations {
    public static int maxLength(Integer[] ss_arrays, int n){
        List <Integer> ss_list = new ArrayList<Integer>(Arrays.asList(ss_arrays));
        Collections.sort(ss_list);
        int maxDist = ss_list.get(0);
        boolean isChanged = false;
        int start = 0, end = 0;
        for(int i=1; i<ss_list.size(); i++){
            if((ss_list.get(i) - ss_list.get(i-1)) >= maxDist){
                start = ss_list.get(i-1);
                end = ss_list.get(i);
                maxDist = ss_list.get(i) - ss_list.get(i-1);
                isChanged = true;
            }
        }
        if(isChanged)
            maxDist /= 2;
        if(((n-1)-ss_list.get(ss_list.size()-1)) > maxDist)
            maxDist = (n-1)-ss_list.get(ss_list.size()-1);
        return maxDist;
    }
    public static void main(String[] args) {
        int n,m;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        m = sc.nextInt();
        if(n==m){
            System.out.println("0");
        }else{
            Integer[] spaceStations = new Integer[m];
            for(int i=0; i<m; i++)
                spaceStations[i] = new Integer(sc.nextInt());
            System.out.println(maxLength(spaceStations, n));
        }
    }
}