import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class LisasWorkbook {
    
    static int numberOfSpecialProblems(int n, int k, int[] t){
        int specialProblemCount=0, pageCount=1, problemCount=0;
        /* TESTING */
        //int iterations=0;
        /*         */
        for(int i=0; i<n; i++){
            for(int j=1; j<=t[i]; j++){
                problemCount++;
                if(problemCount == (k+1)){
                    pageCount++;
                    problemCount=1;
                }
                if(j == pageCount)
                    specialProblemCount++;
                //System.out.println("\nITERATION "+(++iterations)+" :\ni = "+i+"\nj = "+j+"\nproblemCount = "+problemCount+"\npageCount = "+pageCount+"\nspecialProblemCount = "+specialProblemCount+"\n");
            }
            problemCount=0;
            pageCount++;
        }
        return specialProblemCount;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n, k;
        int t[];
        n = sc.nextInt();
        k = sc.nextInt();
        t = new int[n];
        for(int i=0; i<n; i++)
            t[i]=sc.nextInt();
        System.out.println(numberOfSpecialProblems(n,k,t));
    }
}