import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class CutTheSticks {
    
    static int arr[];
    static int n;
    static void iterate(){
        while(!allEmpty()){
            remainingSticks();
            cutSticks(findMin());
        }
    }
    static void cutSticks(int index){
        int value=arr[index];
        for(int i=0;i < n;i++)
            if(arr[i]>0)
                arr[i]=arr[i]-value;
    }
    static void remainingSticks(){
        int count=0;
        for(int i=0; i< n;i++)
            if(arr[i]>0)
                count++;
        System.out.println(count);
    }
    static int findMin(){
        int index=0;
        int min_value=9999;
        for(int i=0; i < n; i++)
            if(arr[i] < min_value && arr[i]!=0){
                index=i;
                min_value=arr[i];
            }
        return index;
    }
    static boolean allEmpty(){
        for(int i=0; i < n; i++)
            if(arr[i]!=0)
                return false;
        return true;
    }
    static void showSticks(){
        for(int i=0; i < n; i++){
            if(arr[i]==0)
                System.out.print(" _ ");
            else
                System.out.print(" "+arr[i]+" ");
        }
        System.out.println();
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        arr = new int[n];
        for(int arr_i=0; arr_i < n; arr_i++){
            arr[arr_i] = in.nextInt();
        }
        iterate();
    }
}