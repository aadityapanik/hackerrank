import java.io.*;
import java.util.*;

public class NewYearChaos {

    public static void main(String[] args) {
        int t;
        Scanner sc = new Scanner(System.in);
        t = sc.nextInt();
        for(int i=0; i<t; i++){
            int n = sc.nextInt();
            int bribes = 0;
            int[] input = new int[n];
            int[] distribution = new int[n+1];
            boolean isSorted = false, isTooChaotic = false;
            for(int j=0; j<n; j++)
                input[j] = sc.nextInt();
            mainloop:
            while(!isSorted){
                isSorted = true;
                for(int j=0; j<(n-1); j++){
                    if(input[j] > input[j+1]){
                        distribution[input[j]]++;
                        if(distribution[input[j]] > 2){
                            isTooChaotic = true;
                            break mainloop;
                        }
                        bribes++;
                        int temp = input[j];
                        input[j] = input[j+1];
                        input[j+1] = temp;
                        isSorted = false;
                    }
                }
            }
            if(isTooChaotic)
                System.out.println("Too chaotic");
            else
                System.out.println(bribes);
        }
    }
}