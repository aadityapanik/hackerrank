import java.util.*;
import java.io.*;

class AbsolutePermutation {
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);
		int t,n,k;
		t = sc.nextInt();
		for (int i=0; i<t; i++) {
			n = sc.nextInt();
			k = sc.nextInt();
			if((n%(2*k) == 0) && ((n/k)%2 == 0)){
				int c = n/(2*k);
				int counter = 1;
				for (int j=0; j<c; j++) {
					for (int l = 0; l<k; l++){
						System.out.print((counter+k) + " ");
						counter++;
					}
					for (int l = 0; l<k; l++){
						System.out.print((counter - k) + " ");
						counter++;
					}
				}
				System.out.println();
			}
			else
				System.out.println("-1");
		}
	}
}